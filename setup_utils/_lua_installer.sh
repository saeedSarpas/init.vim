#!/usr/bin/env bash

luainstallerdir=$(dirname "${BASH_SOURCE[0]}")
source ${luainstallerdir}/logging.sh;

function _lua_install() {
  if [[ -n "$1" ]]; then local _lua_pkg="$1"; else err "_lua_install needs exactly one argument"; return; fi;

  if command -v luarocks &> /dev/null
  then
    log "luarocks install ${_lua_pkg}";
    luarocks install ${_lua_pkg} 2>&1 | log_stdout;
  fi
}
