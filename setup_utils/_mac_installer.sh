#!/usr/bin/env bash

macinstallerdir=$(dirname "${BASH_SOURCE[0]}");
source ${macinstallerdir}/logging.sh;

function _mac_install() {
  if [[ -n "$1" ]]; then local _brew="$1"; else err "_mac_install need exactly 2 arguments!"; return; fi;

  # TODO: check for other package managers, e.g. MacPorts
  _brew_install ${_brew};
}

function _brew_install() {
  if [[ -n "$1" ]]; then local _brew="$1"; else err "_brew_install needs exactly 1 argument!"; return; fi

  if [ "${_brew}" = "_" ]; then
    err "I skip installation as you have not proveded me with a package name!";
    return
  fi

  if command -v ${_brew} &> /dev/null; then
    log "${_brew} is already installed";
    return
  fi

  if command -v brew &> /dev/null; then
    log "brew install ${_brew}"
    brew install ${_brew} 2>&1 | log_stdout;
    exec_status $?;
  else
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)" 2>&1 | log_stdout;
    exec_status $?;
  fi
}
