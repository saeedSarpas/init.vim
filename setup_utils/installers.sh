#!/usr/bin/env bash

installerdir=$(dirname "${BASH_SOURCE[0]}");
source ${installerdir}/_linux_installer.sh;
source ${installerdir}/_mac_installer.sh;
source ${installerdir}/_pip_installer.sh;
source ${installerdir}/_npm_installer.sh;
source ${installerdir}/_gem_installer.sh;
source ${installerdir}/_lua_installer.sh;
source ${installerdir}/logging.sh;

function install() {
  if [[ -n "$1" ]]; then local _arch="$1"; else err "install needs exactly three arguments"; return; fi;
  if [[ -n "$2" ]]; then local _ubun="$2"; else err "install needs exactly three arguments"; return; fi;
  if [[ -n "$3" ]]; then local _brew="$3"; else err "install needs exactly three arguments"; return; fi;

  section "Install a new package";

  local _unameOut="$(uname -s)";
  case "${_unameOut}" in
    Linux*)     machine=Linux;;
    Darwin*)    machine=Mac;;
    *)          err "Unknown OS! (${_unameOut})"; return;;
  esac

  case $machine in
    "Linux")    _linux_install $_arch $_ubun;;
    "Mac")      _mac_install $_brew;;
  esac
}

function python_install() {
  if [[ -n "$1" ]]; then local _py_pkg="$1"; else err "python_install needs exactly one argument"; return; fi;

  _pip2_install ${_py_pkg};
  _pip3_install ${_py_pkg};
}

function npm_install() {
  if [[ -n "$1" ]]; then local _npm_pkg="$1"; else err "_npm_install needs exactly one argument"; return; fi;

  _npm_install ${_npm_pkg};
}

function gem_install() {
  if [[ -n "$1" ]]; then local _gem_pkg="$1"; else err "_gem_install needs exactly one argument"; return; fi;

  _gem_install ${_gem_pkg};
}

function lua_install() {
  if [[ -n "$1" ]]; then local _lua_pkg="$1"; else err "_lua_install needs exactly one argument"; return; fi;

  _lua_install ${_lua_pkg};
}
