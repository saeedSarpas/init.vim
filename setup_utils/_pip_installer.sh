#!/usr/bin/env bash

pipinstallerdir=$(dirname "${BASH_SOURCE[0]}");
source ${pipinstallerdir}/logging.sh;

function _pip2_install() {
  if [[ -n "$1" ]]; then local _py_pkg="$1"; else err "_pip2_install needs exactly one argument"; return; fi;

  if command -v pip2 &> /dev/null
  then
    log "pip2 install --upgrade ${_py_pkg}"
    pip2 install --upgrade ${_py_pkg} 2>&1 | log_stdout;
  fi
}

function _pip3_install() {
  if [[ -n "$1" ]]; then local _py_pkg="$1"; else err "_pip3_install needs exactly one argument"; return; fi;

  if command -v pip3 &> /dev/null
  then
    log "pip3 install --upgrade ${_py_pkg}"
    pip3 install --upgrade ${_py_pkg} 2>&1 | log_stdout;
  fi
}
