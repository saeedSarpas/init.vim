#!/usr/bin/env bash

geminstallerdir=$(dirname "${BASH_SOURCE[0]}");
source ${geminstallerdir}/logging.sh;

function _gem_install() {
  if [[ -n "$1" ]]; then local _gem_pkg="$1"; else err "_gem_install needs exactly one argument"; return; fi;

  if command -v gem &> /dev/null
  then
    log "gem install --user-install ${_gem_pkg}";
    gem install --user-install ${_gem_pkg} 2>&1 | log_stdout;
  fi
}
