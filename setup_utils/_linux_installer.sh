#!/usr/bin/env bash

linuxinstallerdir=$(dirname "${BASH_SOURCE[0]}");
source ${linuxinstallerdir}/logging.sh;

function _linux_install() {
  if [[ -n "$1" ]]; then local _arch="$1"; else err "_linux_install needs exactly 2 arguments!"; return; fi;
  if [[ -n "$2" ]]; then local _ubun="$2"; else err "_linux_install needs exactly 2 arguments!"; return; fi;

  local _distro="$(lsb_release -i | cut -f 2-)";

  case "${_distro}" in
    Arch*)       _yay_install "${_arch}";;
    Ubuntu*)     _apt_install "${_ubun}";;
    *)           log "Unknown distro: ${_distro}"
  esac
}

function _install_yay() {
  log "Installing yay";
  cd /opt;
  sudo git clone https://aur.archlinux.org/yay-git.git;
  sudo chown -R $(whoami):$(whoami) ./yay-git;
  cd yay-git;
  makepkg -si;
}

function _yay_install() {
  if [[ -n "$1" ]]; then local _arch="$1"; else log "_yay_install needs exactly one arguments"; return; fi

  if [ "${_arch}" = "_" ]; then
    err "I skip installation as you have not proveded me with a package name!";
    return
  fi

  if command -v ${_arch} &> /dev/null; then
    log "${_arch} is already installed";
    return
  fi

  if !command -v yay &> /dev/null; then
    local cwd=$(pwd);
    install_yay;
    local $_status=$?;
    exec_status ${_status};
    cd ${cwd};
  fi

  if [ "${_status}" -eq 0 ]; then
    log "sudo yay install ${_arch}";
    sudo yay -S ${_arch} 2>&1 | log_stdout;
    exec_status $?;
  fi
}

function _apt_install() {
  if [[ -n "$1" ]]; then local _ubun="$1"; else log "_apt_install needs exactly one arguments"; return; fi;

  if [ "${_ubun}" = "_" ]; then
    err "I skip installation as you have not proveded me with a package name!";
    return
  fi

  if command -v ${_ubun} &> /dev/null; then
    log "${_ubun} is already installed";
    return
  fi

  log "sudo apt-get install ${_ubun}";
  sudo apt-get install ${_ubun} 2>&1 | log_stdout;
  exec_status $?;
}
