#!/usr/bin/env bash

npminstallerdir=$(dirname "${BASH_SOURCE[0]}");
source ${npminstallerdir}/logging.sh;

function _npm_install() {
  if [[ -n "$1" ]]; then local _npm_pkg="$1"; else err "_npm_install needs exactly one argument"; return; fi;

  if command -v npm &> /dev/null
  then
    log "npm install -g ${_npm_pkg}";
    npm install -g ${_npm_pkg} 2>&1 | log_stdout;
  fi
}
