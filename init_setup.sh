#!/usr/bin/env bash

here=$(dirname "${BASH_SOURCE[0]}");
source ${here}/setup_utils/installers.sh
source ${here}/setup_utils/logging.sh

function prerequisite_software() {
  section "Installing prerequisite packages"

  install wget wget wget
  install unzip unzip unzip
  install python-pip python3-pip python
  install nodejs nodejs node
  install npm npm _
  install ruby ruby _
  install rubygems _ _
  install lua lua5.2 lua
}

function essential_packages() {
  python_install "neovim";
  npm_install "neovim";
  gem_install "neovim";
}

function extra_packages() {
  lua_install "digestif";
  python_install "yalafi";
}

function language_tool() {
  section "LanguageTool";
  log "Installing LanguageTool";

  local cwd=$(pwd);

  if [ -d "${HOME}/.LanguageTool" ]; then
    mv -v "${HOME}/.LanguageTool" "${HOME}/.LanguageTool_$(date +"%Y%m%d%H%M%S")";
  fi

  wget https://languagetool.org/download/LanguageTool-stable.zip 2>&1 | log_stdout;
  return_if_failed $?

  local parent_dir_name=$(unzip -Z -1 LanguageTool-stable.zip | head -n 1);
  unzip LanguageTool-stable.zip 2>&1 | log_stdout;
  return_if_failed $?

  mv ${parent_dir_name} ${HOME}/.LanguageTool;

  rm -v ${cwd}/LanguageTool-stable.zip 2>&1 | log_stdout;

  mkdir -vp ${HOME}/.vlty 2>&1 | log_stdout;
  touch $HOME/.vlty/defs.tex;
  touch $HOME/.vlty/repls.txt;
}

function main() {
  while true; do
    echo "Use one of the following actions:"
    echo "    e: essential packages"
    echo "    l: LanguageTool"
    echo "    p: prerequisite software"
    echo "    x: extra packages"
    echo "    q: quit"
    echo ""

    read -p 'Action? [e,p,l,q] ' action
    case $action in
      [Ee]) essential_packages;;
      [Ll]) language_tool;;
      [Pp]) prerequisite_software;;
      [Xx]) extra_packages;;
      [Qq]) exit;;
      *) continue;;
    esac
  done
}

if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
    main "$@"
fi
