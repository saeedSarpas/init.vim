" Window configs
nnoremap <tab> <C-w><C-w>
let g:which_key_map.w['w'] = ['<C-W>w', 'next-window']
let g:which_key_map.w['/'] = ['<C-W>v', 'split-window-right']
let g:which_key_map.w['-'] = ['<C-W>s', 'split-window-below']
let g:which_key_map.w['h'] = ['<C-W>h', 'window-left']
let g:which_key_map.w['j'] = ['<C-W>j', 'window-below']
let g:which_key_map.w['k'] = ['<C-W>k', 'window-up']
let g:which_key_map.w['l'] = ['<C-W>l', 'window-right']
let g:which_key_map.w['H'] = ['<C-W>H', 'move-window-left']
let g:which_key_map.w['J'] = ['<C-W>J', 'move-window-below']
let g:which_key_map.w['K'] = ['<C-W>K', 'move-window-up']
let g:which_key_map.w['L'] = ['<C-W>L', 'move-window-right']
let g:which_key_map.w['>'] = ['<C-W>20>', 'expand-window-right']
let g:which_key_map.w['<'] = ['<C-W>20<', 'expand-window-left']
let g:which_key_map.w['d'] = ['<C-W>c', 'delete-window']
let g:which_key_map.w['='] = ['<C-W>=', 'balance-window']

