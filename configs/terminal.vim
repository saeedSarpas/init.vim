" Terminal configs
set termguicolors
let g:which_key_map.e["'"] = [':split term://zsh', 'open-terminal']
tnoremap jk <C-\><C-n>
tnoremap <SPACE>wd <C-\><C-n> j
