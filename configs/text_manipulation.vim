" Text manipulation configs
filetype plugin on
vmap <SPACE>xl <Plug>NERDCommenterToggle
let g:which_key_map.x['l'] = ['<Plug>NERDCommenterToggle', 'toggle-comment']
let g:NERDCreateDefaultMappings = 0
let g:NERDSpaceDelims = 1
let g:NERDCompactSexyComs = 1
let g:NERDDefaultAlign = 'left'
let g:NERDAltDelims_java = 1
let g:NERDCustomDelimiters = { 'c': { 'left': '/**','right': '*/' } }
let g:NERDCommentEmptyLines = 1
let g:NERDTrimTrailingWhitespace = 1
let g:NERDToggleCheckAllLines = 1

" Text
vnoremap u ~
vnoremap ) c(<ESC>p`]a)<ESC>
vnoremap } c{<ESC>p`]a}<ESC>
vnoremap ] c[<ESC>p`]a]<ESC>
let g:which_key_map.x['e'] = ['zr', 'expand-all']

