" Syntax configs
lua << EOF
require'nvim-treesitter.configs'.setup {
  -- A list of parser names, or "all" (the five listed parsers should always be installed)
  ensure_installed = { "c", "cpp", "lua", "vim", "vimdoc", "query", "bash", "cmake", "css", "dockerfile", "fortran", "git_config", "http", "java", "javascript", "julia", "kotlin", "latex", "go", "python", "regex", "ruby", "scala", "sql", "tsx", "typescript", "yaml" },
  sync_install = false,
  auto_install = true,
  ignore_install = {},
  highlight = {
    enable = true,
    disable = {},
    disable = function(lang, buf)
        local max_filesize = 1024 * 1024
        local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(buf))
        if ok and stats and stats.size > max_filesize then
            return true
        end
    end,

    additional_vim_regex_highlighting = false,
  },
}
EOF


" Custom syntax highlighting
autocmd BufNewFile,BufRead *.jenkins set syntax=groovy
