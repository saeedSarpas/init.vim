" Git configs

let g:which_key_map.g['c'] = [':Telescope git_commits', 'commits']
let g:which_key_map.g['b'] = [':Telescope git_branches', 'branches']
let g:which_key_map.g['s'] = [':Telescope git_status', 'status']
let g:which_key_map.g['h'] = [':Telescope git_stash', 'stash']
let g:which_key_map.g['m'] = [':Git blame', 'blame']
let g:which_key_map.g['d'] = ['Gdiff', 'diff']
let g:which_key_map.g['l'] = ['Gclog', 'log']

let g:which_key_map.g.t['l'] = ['0Gclog', 'log']
let g:which_key_map.g.t['c'] = [':Telescope git_bcommits', 'commits']
