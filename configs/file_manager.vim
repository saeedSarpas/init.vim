" File manager configs
let g:which_key_map.f['f'] = [':NvimTreeToggle', 'File tree']

lua << EOF
require'nvim-tree'.setup {
  on_attach           = on_attach,
  disable_netrw       = true,
  hijack_netrw        = true,
  open_on_tab         = false,
  hijack_cursor       = false,
  update_cwd          = false,
  diagnostics = {
    enable = true,
    icons = {
      hint = "",
      info = "",
      warning = "",
      error = "",
    }
  },
  update_focused_file = {
    enable      = false,
    update_cwd  = false,
    ignore_list = {}
  },
  system_open = {
    cmd  = nil,
    args = {}
  },
  filters = {
    dotfiles = false,
    custom = {}
  },
  view = {
    width = 40,
    side = 'right',
  },
  git = {
      ignore = false,
  }
}

