" Easymotion configs
let g:EasyMotion_do_mapping = 1
let g:which_key_map.x['j'] = ['<Plug>(easymotion-overwin-f)', 'easy-motion']

runtime after/colors/custom_highlight_groups.vim

highlight EasyMotionTarget ctermbg=none ctermfg=green
highlight link EasyMotionShade Invisible
highlight EasyMotionTarget2First ctermbg=none ctermfg=red
highlight EasyMotionTarget2Second ctermbg=none ctermfg=lightred
highlight EasyMotionMoveHL ctermbg=green ctermfg=black
highlight EasyMotionIncSearch ctermbg=green ctermfg=black

