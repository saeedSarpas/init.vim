" Buffer configs
for i in range(1, 99)
  let g:which_key_map.b[i] = ["buffer " . i . "<CR>", "buffer-" . i]
endfor

let g:which_key_map.b['p'] = ['bprevious', 'previous-buffer']
let g:which_key_map.b['n'] = ['bnext', 'next-buffer']
let g:which_key_map.b['f'] = ['bfirst', 'first-buffer']
let g:which_key_map.b['l'] = ['blast', 'last-buffer']
let g:which_key_map.b['d'] = ['bdelete', 'delete-buffer']
let g:bufferline_active_buffer_left = '['
let g:bufferline_active_buffer_right = ']'
let g:bufferline_modified = '*'
let g:bufferline_show_bufnr = 1
let g:bufferline_fname_mod = ':t'
let g:bufferline_excludes = ['.']
