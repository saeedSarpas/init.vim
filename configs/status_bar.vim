
" Status bar configs
" To config check this: https://github.com/vim-airline/vim-airline#configurable-and-extensible
let g:airline_theme='deus'
let g:airline_statusline_ontop=0
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline#extensions#tabline#formatter = 'unique_tail'
let g:airline#extensions#bufferline#enabled = 1
let g:airline#extensions#bufferline#overwrite_variables = 1
