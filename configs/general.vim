" Syntax highlighting
syntax on

" Line number configs
set number
set relativenumber
set signcolumn=yes

" Line ending
set list listchars=tab:>\ ,trail:-,eol:$

" Search configs
set smartcase

" Mouse configs
set mouse=a
set hidden

" Command and status line configs
set cmdheight=1
set shortmess+=c

" Swap and cursor hold configs
set updatetime=300

" GUI configs
set guifont=Inconsolata\ 13

" White space configs
set tabstop=2
set shiftwidth=2
set expandtab

" Splitting configs
set splitbelow
set splitright

" Cursor configs
inoremap jk <ESC>
nnoremap 0 ^
nnoremap ^ 0

" Clipboard
" Make sure to install xclip
set clipboard+=unnamedplus

" Remove trailig spaces before save
autocmd BufWritePre * :%s/\s\+$//e

" Editor configs
function! LineNumberToggle()
  if(&relativenumber == 1)
    set number
    set norelativenumber
  else
    set number
    set relativenumber
  endif
endfunc

function! WrapToggle()
  if(&wrap == 1)
    set nowrap
  else
    set wrap
  endif
endfunc

let g:which_key_map.e['n'] = [':call LineNumberToggle()', 'toggle-line-number']
let g:which_key_map.e['R'] = [':so $MYVIMRC', 'refresh']
let g:which_key_map.e['w'] = [':call WrapToggle()', 'toggle-line-wrap']

