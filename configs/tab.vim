" Tab configs
let g:which_key_map.t['t'] = ['tabnew', 'new-tab']
let g:which_key_map.t['n'] = ['tabnext', 'tab-next']
let g:which_key_map.t['p'] = ['tabpre', 'tab-prev']
let g:which_key_map.t['c'] = ['tabclose', 'tab-close']
