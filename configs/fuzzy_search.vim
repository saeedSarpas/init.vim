" Fuzzy search configs

lua << EOF
require('telescope').setup {
  extensions = {
    fzf = {
      fuzzy = true,
      override_generic_sorter = true,
      override_file_sorter = true,
      case_mode = "smart_case",
    }
  }
}
require('telescope').load_extension('fzf')
EOF

let g:which_key_map.f['/'] = [':Telescope find_files', 'Search files']
let g:which_key_map.f['g'] = [':Telescope live_grep', 'Live grep']
let g:which_key_map.f['b'] = [':Telescope buffers', 'Find buffer']
let g:which_key_map.f['h'] = [':Telescope help_tags', 'Help tags']
let g:which_key_map.f['c'] = [':Telescope colorscheme', 'List colorschemes']
let g:which_key_map.f['s'] = ['w', 'save-file']
