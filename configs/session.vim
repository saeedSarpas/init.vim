" Sessions configs
let g:which_key_map.s['l'] = ['SLoad', 'session-load']
let g:which_key_map.s['s'] = ['SSave', 'session-save']
let g:which_key_map.s['d'] = ['SDelete', 'session-delete']
let g:which_key_map.s['c'] = ['SClose', 'session-close']
