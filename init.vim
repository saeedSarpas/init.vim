" Saeed's Neovim configs
" Run: :checkhealth



" Vim-Plug
if has('nvim')
  call plug#begin('~/.local/share/nvim/plugged')
else
  call plug#begin('~/.vim/bundle')
endif


" Whichkey
Plug 'liuchengxu/vim-which-key'

" Buffer
Plug 'bling/vim-bufferline'

" Status bar
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" Fuzzy search
Plug 'BurntSushi/ripgrep'
Plug 'nvim-telescope/telescope-fzf-native.nvim', { 'do': 'make' }
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'

" Fuzzy finder
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

" Git
Plug 'tpope/vim-fugitive'

" Text manipulation
Plug 'preservim/nerdcommenter'

" Startify
Plug 'mhinz/vim-startify'

" LaTeX
" NB: Can be replaced by Texlab
Plug 'astoff/digestif'

" Javascript
Plug 'pangloss/vim-javascript'
Plug 'MaxMEllon/vim-jsx-pretty'

" Typescript
Plug 'leafgarland/typescript-vim'
Plug 'peitalin/vim-jsx-typescript'

" Surround
Plug 'tpope/vim-surround'
Plug 'tpope/vim-repeat'
Plug 'jiangmiao/auto-pairs'

" Text navigation
Plug 'easymotion/vim-easymotion'

" Debugger
Plug 'sakhnik/nvim-gdb', { 'do': ':!./install.sh \| UpdateRemotePlugins' }

" LSP
Plug 'neovim/nvim-lspconfig'
Plug 'kabouzeid/nvim-lspinstall'

" Lsp completion
Plug 'glepnir/lspsaga.nvim'

" Syntax
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'windwp/nvim-ts-autotag'
Plug 'p00f/nvim-ts-rainbow'
Plug 'nvim-treesitter/nvim-treesitter-refactor'

" Autocompletion
Plug 'hrsh7th/nvim-compe'

" OpenAPI
Plug 'hsanson/vim-openapi'


" File manager
Plug 'kyazdani42/nvim-web-devicons'
Plug 'kyazdani42/nvim-tree.lua'


" Latex
Plug 'lervag/vimtex'


" AI completion
Plug 'github/copilot.vim'



" Colorschemes
Plug 'nordtheme/vim'
Plug 'ajmwagar/vim-deus'
Plug 'axvr/photon.vim'
Plug 'bluz71/vim-nightfly-guicolors'
Plug 'cocopon/iceberg.vim'
Plug 'dracula/vim'
Plug 'embark-theme/vim', { 'as': 'embark' }
Plug 'frenzyexists/aquarium-vim', { 'branch': 'develop' }
Plug 'heraldofsolace/nisha-vim'
Plug 'jacoborus/tender.vim'
Plug 'joshdick/onedark.vim'
Plug 'lifepillar/vim-solarized8'
Plug 'lighthaus-theme/vim-lighthaus'
Plug 'ozkanonur/nimda.vim'
Plug 'rakr/vim-one'
Plug 'sainnhe/everforest'
Plug 'sainnhe/sonokai'
Plug 'Shatur/neovim-ayu'
Plug 'sonph/onehalf', {'rtp': 'vim/'}
Plug 'wadackel/vim-dogrun'

call plug#end()



" Whichkey configs
set timeoutlen=500
let g:mapleader = "\<Space>"
nnoremap <silent> <leader> :WhichKey '<Space>'<CR>

let g:which_key_map =  {}
let g:which_key_map.b = {'name': '+buffers'}
let g:which_key_map.d = {'name': '+debugger'}
let g:which_key_map.e = {'name': '+editor'}
let g:which_key_map.f = {'name': '+files'}
let g:which_key_map.g = {'name': '+git'}
let g:which_key_map.g['t'] = {'name': '+thisfile'}
let g:which_key_map.l = {'name': '+lsp'}
let g:which_key_map.m = {'name': '+marks'}
let g:which_key_map.s = {'name': '+sessions'}
let g:which_key_map.t = {'name': '+tabs'}
let g:which_key_map.w = {'name': '+windows'}
let g:which_key_map.x = {'name': '+text'}

call which_key#register(' ', "g:which_key_map")

autocmd FileType which_key highlight WhichKeyFloating guibg=12 guifg=7 ctermbg=12 ctermfg=7



" Configs
runtime configs/general.vim
runtime configs/package_manager.vim
runtime configs/buffer.vim
runtime configs/status_bar.vim
runtime configs/color_scheme.vim
runtime configs/fuzzy_search.vim
runtime configs/git.vim
runtime configs/text_manipulation.vim
runtime configs/session.vim
runtime configs/text_navigation.vim
runtime configs/debugger.vim
runtime configs/lsp.vim
runtime configs/syntax.vim
runtime configs/autocompletion.vim
runtime configs/file_manager.vim
runtime configs/terminal.vim
runtime configs/window.vim
runtime configs/tab.vim




" Tex/pdf viewer configs
let g:vimtex_view_general_viewer = 'zathura'
let g:tex_flavor='latex'
let g:vimtex_quickfix_mode=0
set conceallevel=1
let g:tex_conceal='abdmg'





" LanguageTool configs
map mc :w <bar> compiler vlty <bar> make <bar> :cw <cr><esc>
let g:tex_flavor = 'latex'
set spelllang=en_gb

" Uncomment below to get the spell checking inside the latex text
" set spellfile=en_gb spell
" set modeline

let g:vimtex_grammar_vlty = {}
let g:vimtex_grammar_vlty.lt_directory = '~/.LanguageTool'
" let g:vimtex_grammar_vlty.lt_command = 'languagetool'
" let g:vimtex_grammar_vlty.server = 'my'
let g:vimtex_grammar_vlty.show_suggestions = 1
let g:vimtex_grammar_vlty.shell_options =
        \   ' --multi-language'
        \ . ' --packages "*"'
        \ . ' --define ~/.vlty/defs.tex'
        \ . ' --replace ~/.vlty/repls.txt'
        \ . ' --equation-punctuation display'
        \ . ' --single-letters "i.\,A.\|z.\,B.\|\|"'





" Copilot configs
" let g:which_key_map.c['s'] = [':Copilot_setup', 'setup']
" let g:which_key_map.c['e'] = [':Copilot_enable', 'enable']
" inoremap <C-0> <C-]> <CR>
" let g:which_key_map.c['d'] = [':Copilot_disable', 'disable']
" let g:which_key_map.c['u'] = [':Copilot_status', 'status']
" let g:which_key_map.c['t'] = ['copolot-i_<Tab>', 'accept-Copilot-suggestion']
" let g:which_key_map.c['n'] = ['copolot-i_CTRL-]', 'next-Copilot-suggestion']
