if !exists('g:loaded_lspsaga') | finish | endif

lua <<EOF
local saga = require 'lspsaga'

saga.init_lsp_saga {
  error_sign = '',
  warn_sign = '',
  hint_sign = '',
  infor_sign = '',
  border_style = "round",
}


EOF

nnoremap <silent> gh :Lspsaga lsp_finder<CR>
nnoremap <silent> gd :Lspsaga preview_definition<CR>

" nnoremap <silent><leader>ca :Lspsaga code_action<CR>
" vnoremap <silent><leader>ca :<C-U>Lspsaga range_code_action<CR>

" nnoremap <silent>K :Lspsaga hover_doc<CR>
" scroll down hover doc or scroll in definition preview
nnoremap <silent> <C-f> <cmd>lua require('lspsaga.action').smart_scroll_with_saga(1)<CR>
" scroll up hover doc
nnoremap <silent> <C-b> <cmd>lua require('lspsaga.action').smart_scroll_with_saga(-1)<CR>

" nnoremap <silent> gs :Lspsaga signature_help<CR>

nnoremap <silent>gr :Lspsaga rename<CR>
" -- close rename win use <C-c> in insert mode or `q` in noremal mode or `:q`

" -- only show diagnostic if cursor is over the area
" nnoremap <silent> <leader>cd :Lspsaga show_line_diagnostics<CR>
" nnoremap <silent> <leader>cc :Lspsaga show_cursor_diagnostics<CR>
